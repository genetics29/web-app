from tkinter import *
import tkinter as tk
import pyodbc as pc


# Create the main window
root = tk.Tk()

# State Variables
global cursor
db_error = tk.StringVar()
db_status = tk.StringVar()
# SQL Setup 
connection_string = (
'DRIVER=MySQL ODBC 8.0 ANSI Driver;'
'SERVER=localhost;'
'Database=genetics_db;'
'UID=py;'
'PASSWORD=sql#Gen29_;'
'charset=utf8mb4;'
) 

# functions
# Create a function for Button 1
def ga_create_button_click():
    # initialize choices
    selection_choices = ('Roulette', 'Elitism')
    crossover_choices = ('Single Point', 'Uniform Binary', 'Custom')

    # State Variables
    selection = tk.StringVar()
    crossover = tk.StringVar()
    max_gens = tk.IntVar()
    mut_scl = tk.DoubleVar()
    mut_rate = tk.DoubleVar()
    pop_count = tk.IntVar()
    elite_amt = tk.IntVar()
    parent_amt = tk.IntVar()

    # Called on create
    def validate_ga():
        print()

    #insert
    def insert():
        query = f'INSERT INTO GENETIC_ALGORITHM(max_generations, selection, crossover, parent_amt, elitist_amt, mutation_scale, mutation_rate, population_count) VALUES ({max_gens.get()},\'{selection.get()}\',\'{crossover.get()}\',{parent_amt.get()},{elite_amt.get()},{mut_scl.get()},{mut_rate.get()},{pop_count.get()})'
        print(query)
        cursor.execute(query)
        #creation_window.destroy()

    # Create grid and outer frame
    creation_window = Toplevel(root)
    creation_window.title("Run a Simulation")
    creation_frame = tk.Frame(creation_window, padx=15,pady=15)
    creation_frame.grid(column=0,row=0,sticky=(N, W, E, S))
    creation_window.grid_columnconfigure(0, weight=1)
    creation_window.grid_rowconfigure(0, weight=1)

    # make Widgets
    lbl_sel = tk.Label(creation_frame, text="Selection:")
    lbl_crs = tk.Label(creation_frame, text="Crossover:")
    lbl_gen = tk.Label(creation_frame, text="Max Generations:")
    lbl_muts = tk.Label(creation_frame, text="Mutation Scale:")
    lbl_mutr = tk.Label(creation_frame, text="Mutation Rate:")
    lbl_pop = tk.Label(creation_frame, text="Population Count:")
    lbl_elite = tk.Label(creation_frame, text="Elitism Amount:")
    lbl_parent = tk.Label(creation_frame, text="Parent Amount:")
    sel_combx = tk.OptionMenu(creation_frame, selection, *selection_choices)
    crs_combx  = tk.OptionMenu(creation_frame, crossover, *crossover_choices)
    gen_entry    = tk.Entry(creation_frame, textvariable=max_gens)
    muts_entry   = tk.Entry(creation_frame, textvariable=mut_scl)
    mutr_entry   = tk.Entry(creation_frame, textvariable=mut_rate)
    pop_entry    = tk.Entry(creation_frame, textvariable=pop_count)
    elite_entry  = tk.Entry(creation_frame, textvariable=elite_amt)
    parent_entry = tk.Entry(creation_frame, textvariable=parent_amt)
    create_button = tk.Button(creation_frame, text="Create", command=insert)

    # grid widgets
    lbl_sel.grid(column= 2, row=1)
    lbl_crs.grid(column= 4, row=1)
    lbl_gen.grid(column= 1, row=3)
    lbl_muts.grid(column=3, row=3)
    lbl_mutr.grid(column=5, row=3)
    lbl_pop.grid(column=1,row=5) 
    lbl_elite.grid(column=3,row=5)
    lbl_parent.grid(column=5,row=5)
    sel_combx.grid(column=2, row=2)
    crs_combx.grid(column=4, row=2) 
    gen_entry.grid(column=1, row =4) 
    muts_entry.grid(column=3, row=4) 
    mutr_entry.grid(column=5, row=4) 
    pop_entry.grid(column=1, row=6) 
    elite_entry.grid(column=3, row=6) 
    parent_entry.grid(column=5,row=6)
    create_button.grid(column=2, columnspan=2,row=7)

# Creates a new window for deletion
# Lists all GAs and allows user to select them for deletion
# Removes them from the db and closes the window
def ga_delete_button_click():
    # Grab set of GAs
    query = "SELECT ga_id, crossover, selection FROM GENETIC_ALGORITHM"
    cursor.execute(query)
    GAs = cursor.fetchall()
    ga_id_list = []
    for GA in GAs:
        ga_id_list.append(f'{GA[0]}: {GA[1]} Selection with {GA[2]} Crossover')

    # State variables
    algorithmns = tk.StringVar(value=ga_id_list)
    amt_selected = tk.StringVar()
    sel_list = tk.StringVar()

    # Called on listbox update
    def showSelected(*args):
        sel = lbox.curselection()
        amt_selected.set(f'You are deleting the following {len(sel)} GAs:')
        selected = []
        for i in sel:
            selected.append(GAs[i][0])
        sel_list.set(selected)
    
    # delete selected GAs from DB
    def deleteGAs(*args):
        sel = lbox.curselection()
        for i in sel:
            query = f'DELETE FROM GENETIC_ALGORITHM WHERE ga_id = {GAs[i][0]}'
            cursor.execute(query)
        del_window.destroy()

    # Create and grid outer frame
    del_window = Toplevel(root)
    del_window.title("Delete a Simulation")
    del_frame = tk.Frame(del_window, padx= 12, pady=5)
    del_frame.grid(column=0,row=0, sticky=(N,W,E,S))
    del_window.grid_columnconfigure(0, weight=1)
    del_window.grid_rowconfigure(0, weight=1)

    # Create Widgets
    lbox = tk.Listbox(del_frame, listvariable=algorithmns, height=10, width=60)
    lbl = tk.Label(del_frame, textvariable=amt_selected)
    delete = tk.Button(del_frame,text="DELETE", command=deleteGAs, default=ACTIVE)
    ls = tk.Label(del_frame, textvariable=sel_list)

    # Grid widgets
    lbox.grid(column=0, row=0, rowspan=20, sticky=(N,W,E,S))
    lbl.grid(column=1, row=0, padx=10, pady=5)
    delete.grid(column=2, row=1, sticky=E)
    ls.grid(column=1, row=1, padx=10, pady=5)
    del_frame.grid_columnconfigure(0, weight=1)
    del_frame.grid_rowconfigure(5, weight=1)

    # Bind events
    lbox.bind('<<ListboxSelect>>', showSelected)

    # Colorize alternating lines of the listbox
    for i in range(0,len(GAs),2):
        lbox.itemconfigure(i, background='#f0f0ff')

    showSelected()

# attempts to connect to the database, updates user on status
def db_connect():
    try:
        cnxn = pc.connect(connection_string, autocommit=True)
        global cursor    
        cursor = cnxn.cursor()
        cnxn.setdecoding(pc.SQL_WCHAR, encoding='utf-8')
        cnxn.setencoding(encoding='utf-8')
        db_status.set('Connected!')
        db_error.set=''
        db_status_label.configure(foreground="#8dc149")
        return (cnxn,cursor)
    except pc.Error as ex:
        sqlstate = ex.args[1]
        db_error.set(sqlstate)
        db_status.set('Not Connected')
        db_status_label.configure(foreground="#f55385")
        return (0, 0)

# Connect to db
def main():
    (cnxn, cursor) = db_connect()
    if cnxn == 0:
        error_frame.grid()
    else:
        error_frame.grid_remove()

# Grid and outer frame
mainframe = tk.Frame(root, padx=3, pady=12)
mainframe.grid(column=0, row=0, sticky=(N,W,E,S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)
# error grid
error_frame = tk.Frame(mainframe)
error_frame.grid(column=3,row=1,sticky=(N,W,E,S))
# Set the window title
root.title("Genetics Application")


#widgets
db_refresh_button = tk.Button(error_frame, text="Retry", command=main)
db_error_label = tk.Label(error_frame, textvariable=db_error)
# Create Button 1
ga_create_button = tk.Button(mainframe, text="Run Genetic Algorithm", command=ga_create_button_click)
# Create Button 2
ga_delete_button = tk.Button(mainframe, text="Delete Genetic Algorithm", command=ga_delete_button_click)
#Connection status
cnxn_status_lbl = tk.Label(mainframe, text="Connection status:")
db_status_label = tk.Label(mainframe, textvariable=db_status)


#grid
db_refresh_button.grid(column=1,row=1,sticky=W)
db_error_label.grid(column=1, row=2,sticky=N)
ga_create_button.grid(column=2,row=2,sticky=E)
ga_delete_button.grid(column=3,row=2,sticky=W)
cnxn_status_lbl.grid(column=1, row=1, sticky=W)
db_status_label.grid(column=2, row=1,sticky=W)

# padding
for child in mainframe.winfo_children():
    child.grid_configure(padx=5, pady=5)   

# Start the main event loop
main()

root.mainloop()