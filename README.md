# Web App

## Schema
Schema can be found in `/app/schema.sql`

## Static Files
Html files can be found in `/app/templates`

## Running the website
When not hosting on a kubernetes cluster, move `main.py` to the parent directory and run it there. Requires your own environment variables.

## Running the Genetic Algorithm

Run GeneticAlgorithmDB.jar with this command:
`java -jar GeneticAlgorithm.jar <NN Structure> <Num Generations> <Num Parents> <Num Elitists> <Mutation Scale> <Mutation Prob> <Map Image Path>`

EX:
`java -jar .\GeneticAlgorithmDB.jar "{9,5}" 20 100 20 10 .2 .002 .\RaceTrack1.png`