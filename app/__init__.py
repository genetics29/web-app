import os
import hashlib as hash
from flask import jsonify, request, redirect, url_for, Flask, render_template, g, send_from_directory, session
from flaskext.mysql import MySQL

app = Flask(__name__)

mysql = MySQL()

app.secret_key = b'1d f7 02 30 a2 ef 62 bb 41 50'
# MySQL configurations
app.config["MYSQL_DATABASE_USER"] = "root"
app.config["MYSQL_DATABASE_PASSWORD"] = os.getenv("db_root_password")
app.config["MYSQL_DATABASE_DB"] = os.getenv("db_name")
app.config["MYSQL_DATABASE_HOST"] = os.getenv("MYSQL_SERVICE_HOST")
app.config["MYSQL_DATABASE_PORT"] = int(os.getenv("MYSQL_SERVICE_PORT"))
mysql.init_app(app)

from . import auth, admin, ga, views
app.register_blueprint(auth.bp)
app.register_blueprint(admin.bp)
app.register_blueprint(ga.bp)
app.register_blueprint(views.bp)

@app.route("/")
def index():
    """Function to test the functionality of the API"""
    user_id = session.get('user_id')
    if user_id is None:
        return render_template('index.html')
    else:
        return redirect(url_for('userHome'))

@app.route("/demo")
def demo():
    return render_template('demo.html')

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/userhome')
@auth.login_required
def userHome():
    num_ga = len(ga.getGAs())
    return render_template('userhome.html', num_ga = num_ga)

@app.errorhandler(401)
def unauthorized_page(error):
    return render_template("errors/401.html"), 401


@app.errorhandler(404)
def page_not_found(error):
    return render_template("errors/404.html"), 404


@app.errorhandler(500)
def server_error_page(error):
    return render_template("errors/500.html"), 500
   
#if __name__ == "__main__":
app.run(host="0.0.0.0", port=5000)