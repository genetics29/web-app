import functools
import hashlib as hash
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from flask import abort

from app import mysql
from app.auth import login_required
from app.ga import getGAs, run_simulation_admin
import sys

bp = Blueprint('admin', __name__, url_prefix='/admin')

def superuser_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        if g.user[2] == 0:
            return abort(401)
        return view(**kwargs)

    return wrapped_view

def getUsers(includeUser=False):
    cursor = mysql.get_db().cursor()
    if includeUser:
        cursor.execute('SELECT username, superuser FROM USERS')
    else:
        cursor.execute('SELECT username, superuser FROM USERS WHERE NOT (username = %s)', g.user[0])
    users = cursor.fetchall()
    cursor.close()
    return users

@bp.route('/users', methods=('GET', 'POST'))
@superuser_required
def manage_users():
    if request.method == 'POST':
        cursor = mysql.get_db().cursor()
        try:
            username = request.form["username"]
            password = request.form["password"]
            isSU = False
            if len(request.form) == 3:
                isSU = True
        
            error = None

            cursor.execute(
                    'SELECT * FROM USERS WHERE username = %s', (username,)
                )
            user = cursor.fetchone()
            
            if user is None:
                sql = "INSERT INTO USERS(username, passphrase, superuser) " \
                "VALUES(%s, %s, %s)"
                data = (username, hash.sha256(password.encode()).hexdigest(), isSU)
                cursor.execute(sql, data)
                mysql.get_db().commit()     
            else: 
                error = 'User already exists.'
            
            if error is None:
                flash("User added successfully!") 
                return redirect(url_for('admin.manage_users'))

            flash(error)
        except Exception as e:
            return render_template('errors/500.html',error = str(e))
        finally:
            cursor.close()
    return render_template('admin/manage_users.html', accounts = getUsers(), accounts2 = getUsers(True))

@bp.route('/users/delete', methods=('GET', 'POST'))
@superuser_required
def delete_users():
    if request.method == 'POST':
        cursor = mysql.get_db().cursor()
        try:
            accts = request.form.getlist('deleteCandidates')
            count = 0
            for acct in accts:
                sql = "DELETE FROM USERS WHERE username=%s"
                cursor.execute(sql, acct)
                mysql.get_db().commit()
                count += 1
            cursor.close()
            flash(f'{count} accounts deleted!')
        except Exception as e:
            return render_template('errors/500.html',error = str(e))
        finally:
            cursor.close()
    return redirect(url_for('admin.manage_users'))


@bp.route('/users/password', methods=('GET', 'POST'))
@superuser_required
def change_password():
    if request.method == 'POST':
        cursor = mysql.get_db().cursor()
        try:
            username = request.form["passwordCandidate"]
            password = request.form["newPassword"]
            sql = "UPDATE USERS " \
                "SET passphrase=%s " \
                "WHERE username=%s"
            data = (hash.sha256(password.encode()).hexdigest(), username)
            cursor.execute(sql, data)
            mysql.get_db().commit()
            flash("Password updated!") 
        except Exception as e:
            return render_template('errors/500.html',error = str(e))
        finally:
            cursor.close()
    return redirect(url_for('admin.manage_users'))

@bp.route('/users/username', methods=('GET', 'POST'))
@superuser_required
def change_username():
    if request.method == 'POST':
        cursor = mysql.get_db().cursor()
        try:
            oldUsername = request.form["usernameCandidate"]
            newUsername = request.form["newUsername"]
            error = None

            cursor.execute(
                    'SELECT * FROM USERS WHERE username = %s', (newUsername,)
                )
            duplicateUser = cursor.fetchone()
            
            if duplicateUser is None:
                sql = "UPDATE USERS " \
                "SET username=%s " \
                "WHERE username=%s"
                data = (newUsername, oldUsername)
                cursor.execute(sql, data)
                mysql.get_db().commit()
            else: 
                error = 'Entered username already exists.'
            
            if error is None:
                flash("Username updated!") 
                return redirect(url_for('admin.manage_users'))

            flash(error)
        except Exception as e:
            return render_template('errors/500.html',error = str(e))
        finally:
            cursor.close()
    return redirect(url_for('admin.manage_users'))

@bp.route('/users/superuser', methods=('GET', 'POST'))
@superuser_required
def make_superuser():
    if request.method == 'POST':
        cursor = mysql.get_db().cursor()
        try:
            username = request.form["superuserCandidate"]
            sql = "UPDATE USERS " \
                "SET superuser=True " \
                "WHERE username=%s"
            cursor.execute(sql, username)
            mysql.get_db().commit()
            flash("User promoted to superuser!") 
        except Exception as e:
            return render_template('errors/500.html',error = str(e))
        finally:
            cursor.close()
    return redirect(url_for('admin.manage_users'))

@bp.route('algorithms', methods=('GET', 'POST'))
@bp.route('/algorithms', methods=('GET', 'POST'))
@superuser_required
def manage_algs():
    if request.method == 'POST':
        cursor = mysql.get_db().cursor()
        try:
            crossover = request.form["crossover"]
            selection = request.form["selection"]
            population_count = request.form["population_count"]
            parent_amt = request.form["parent_amt"]
            elitist_amt = request.form["elitist_amt"]
            max_generations = request.form["max_generations"]
            mutation_scale = request.form["mutation_scale"]
            mutation_rate = request.form["mutation_rate"]

            error = None

            sql = "SELECT * FROM GENETIC_ALGORITHMS WHERE crossover=%s AND selection=%s"
            cursor.execute(sql,(crossover, selection))
            alg = cursor.fetchone()

            if alg is None:
                sql = "INSERT INTO GENETIC_ALGORITHMS(max_generations, selection, crossover, parent_amt, elitist_amt, mutation_scale, mutation_rate, population_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                data = (int(max_generations), 
                                     selection, crossover, 
                                     int(parent_amt), 
                                     int(elitist_amt), 
                                     float(mutation_scale), 
                                     float(mutation_rate), 
                                     int(population_count))
                cursor.execute(sql, data)
                gaQuery = "SELECT LAST_INSERT_ID();"
                cursor.execute(gaQuery)
                ga_id = cursor.fetchone()[0]
                mysql.get_db().commit() 
                definQ = "INSERT INTO GA_DEFINES(username, fk_ga_id) VALUES(%s,%s)"
                cursor.execute(definQ, (g.user[0], ga_id))
                mysql.get_db().commit()
            else: 
                error = "Algorithm Already Exists! Delete the old one to create this one."
            
            if error is None:
                flash("Algorithm added successfully!") 
                return redirect(url_for('admin.manage_algs'))

            flash(error)      
        except Exception as e:
            return render_template('errors/500.html',error = str(e))
        finally:
            cursor.close()
    return render_template('admin/manage_ga.html', GAs = getGAs())

@bp.route('/algorithms/run', methods=('GET', 'POST'))
@superuser_required
def run_alg():
    if request.method == 'POST':
        cursor = mysql.get_db().cursor()
        try:
            crossover = request.form["crossover"]
            selection = request.form["selection"]
            population_count = request.form["population_count"]
            parent_amt = request.form["parent_amt"]
            elitist_amt = request.form["elitist_amt"]
            max_generations = request.form["max_generations"]
            mutation_scale = request.form["mutation_scale"]
            mutation_rate = request.form["mutation_rate"]

            error = None

            sql = "SELECT * FROM GENETIC_ALGORITHMS WHERE crossover=%s AND selection=%s"
            cursor.execute(sql,(crossover, selection))
            alg = cursor.fetchone()

            if alg is None:
                error = run_simulation_admin(int(max_generations), 
                                     selection, crossover, 
                                     int(parent_amt), 
                                     int(elitist_amt), 
                                     float(mutation_scale), 
                                     float(mutation_rate), 
                                     int(population_count)) 
            else: 
                error = "Algorithm Already Exists! Delete the old one to create this one."
            
            if error is None:
                flash("Algorithm added successfully! Beginning to Run!") 
                return redirect(url_for('admin.manage_algs'))

            flash(error)      
        except Exception as e:
            return render_template('errors/500.html',error = str(e))
        finally:
            cursor.close()
    return redirect(url_for('admin.manage_algs'))

@bp.route('/algorithms/delete', methods=('GET', 'POST'))
@superuser_required
def delete_algs():
    if request.method == 'POST':
        cursor = mysql.get_db().cursor()
        try:
            GAs = request.form.getlist('deleteCanidates')
            count = 0
            for GA in GAs:
                # grab NNs
                grabNNs = "DELETE FROM neural_networks WHERE nn_id IN (\
                        SELECT indiv_id FROM GENERATIONS WHERE fk_pop_id IN (\
                        SELECT pop_id FROM POPULATIONS WHERE fk_ga_id=%s))"
                cursor.execute(grabNNs, GA)
                mysql.get_db().commit()
                sql = "DELETE FROM GENETIC_ALGORITHMS WHERE ga_id=%s"
                cursor.execute(sql, GA)
                mysql.get_db().commit()
                count += 1
            cursor.close()
            flash(f'{count} Algorithms deleted!')
        except Exception as e:
            return render_template('errors/500.html',error = str(e))
        finally:
            cursor.close()
    return redirect(url_for('admin.manage_algs'))