import functools
import hashlib as hash
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)

from app import mysql

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        db = mysql.get_db().cursor()
        try:
            username = request.form['username']
            password = request.form['password']
        
            error = None
            db.execute(
                'SELECT * FROM USERS WHERE username = %s', (username,)
            )
            user = db.fetchone()
            
            if user is None:
                error = 'Incorrect username.'
                #return render_template('error.html',error = 'Wrong Username or Password')
            elif user[1] != hash.sha256(password.encode()).hexdigest():
                error = 'Incorrect password.'
                #return render_template('error.html',error = 'Wrong Username or Password')

            if error is None:
                session.clear()
                session['user_id'] = user[0]
                session['superuser'] = user[2]
                return redirect(url_for('index'))

            flash(error)
        except Exception as e:
            return render_template('errors/500.html',error = str(e))
        finally:
            db.close()

    return render_template('auth/login.html')

@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')
    
    if user_id is None:
        g.user = None
    else:
        db = mysql.get_db().cursor()
        db.execute(
            'SELECT * FROM USERS WHERE username = %s', (user_id,)
        )
        g.user = db.fetchone()

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view

@bp.route('/logout')
@login_required
def logout():
    session.clear()
    flash("You were logged out.", "success")
    return redirect('/')