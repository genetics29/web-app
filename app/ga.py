import functools
import hashlib as hash
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from flask import abort

from app import mysql
from app.auth import login_required
import sys, os, subprocess

bp = Blueprint('ga', __name__, url_prefix='/app')
gaPath = './app/ga_program/'

def run_simulation_admin(max_generations, selection, crossover, parent_amt, elitist_amt, mutation_scale, mutation_rate, population_count):
    '''
    This can only be called by people who are admins. It should not be accessible for generic users
    '''

    if (max_generations <= 0): return "Invalid max generations"
    if (parent_amt > population_count): return "Invalid number of parents"
    if (elitist_amt > population_count): return "Invalid number of elitists"

    command = "java -jar GeneticAlgorithmDB.jar -ng {} -sel {} -cross {} -np {} -ne {} -ms {} -mr {} -ps {} -map RaceTrack1.png"
    command = command.format(max_generations, selection, crossover, parent_amt, elitist_amt, mutation_scale, mutation_rate, population_count)  
    output = ''
    try:
        print(command)
        java = subprocess.run(command, text=True, capture_output=True, cwd=gaPath)
        if (java.stderr):
            raise Exception(str(java.stderr))
        python = subprocess.run(f'python CSVtoSQL.py {g.user[0]}', text=True, capture_output=True, cwd=gaPath)
        if (python.stderr):
            raise Exception(str(python.stderr))
        output = python.stdout
    except Exception as e:
        return str(e)
    return output

def run_simulation_user(max_generations, population_count):
    '''
    This is the user GA creation ability. It is limited to keep hyperparameters more simple.
    It has less control, which is better for people with less involvement
    '''
    if (max_generations <= 0): return "Invalid max generations"
    if (population_count < 10): return "There must be 10 or more individuals per population"

    run_simulation_admin(max_generations, 'roulette', 'uniform_binary', int(.2 * population_count), int(.1 * population_count), .2, .002, population_count)

def getGAs():
    cursor = mysql.get_db().cursor()
    query = "SELECT * FROM GENETIC_ALGORITHMS"
    cursor.execute(query)
    GAs = cursor.fetchall()
    return GAs

@bp.route('runner', methods=('GET', 'POST'))
@login_required
def run_algorithm():
    if request.method == 'POST':
        cursor = mysql.get_db().cursor()
        try:
            if(len(request.form) == 0):
                return redirect(url_for('ga.run_algorithm'))
            
            ga_id = request.form["ga_id"]
            query = "SELECT * FROM GENETIC_ALGORITHMS WHERE ga_id=%s"
            cursor.execute(query, ga_id)
            ga = cursor.fetchone()
            
            if ga is None:
                return redirect(url_for('ga.run_algorithm'))
            else:
                query = "SELECT * FROM POPULATIONS WHERE fk_ga_id=%s"
                cursor.execute(query, ga_id)
                popAmount = len(cursor.fetchall())
                if popAmount > 0:
                    flash("Algorithm already ran! Ask an admin to delete.")
                    return redirect(url_for('ga.run_algorithm'))
                else:
                    error = run_simulation_admin(crossover=ga[1],
                                         selection=ga[2],
                                         max_generations=ga[3],
                                         mutation_scale=ga[4],
                                         mutation_rate=ga[5],
                                         population_count=ga[6],
                                         elitist_amt=ga[7],
                                         parent_amt=ga[7])
                if error is None:
                    flash("Algorithm added successfully! Beginning to Run!")
                    return redirect(url_for('ga.run_algorithm'))
            flash(error) 
        except Exception as e:
            return render_template('errors/500.html',error = str(e))
        finally:
            cursor.close()
    return render_template('ga/runner.html', GAs = getGAs())