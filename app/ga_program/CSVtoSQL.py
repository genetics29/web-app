#!/usr/bin/python3

import pymysql, pymysql.cursors
import csv, os, sys

cnxn = pymysql.connect(host=os.getenv("MYSQL_SERVICE_HOST"), 
                  database=os.getenv("db_name"), 
                  user="root", 
                  password=os.getenv("db_root_password"),
                  charset="utf8mb4",
                  cursorclass=pymysql.cursors.DictCursor)    

cursor=cnxn.cursor()

with open('DB_Data.csv', newline='') as csvfile:
    file = csv.reader(csvfile, delimiter=',', quotechar='"')

    #Each line: gen#, mapPath, Type, Structure, Reward, Weights
    counter = 1
    ga_id = 0
    nn_id = 0
    try:
        for line in file:
            if (counter == 1):
                query = "INSERT IGNORE INTO GENETIC_ALGORITHMS(max_generations, selection, crossover, parent_amt, elitist_amt, mutation_scale, mutation_rate, population_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                cursor.execute(query, (line[0], line[1], line[2], line[3], line[4], line[5], line[6], line[7]))
                counter += 1
                gaQuery = "SELECT LAST_INSERT_ID();"
                cursor.execute(gaQuery)
                ga_id = cursor.fetchone()['LAST_INSERT_ID()']
                if ga_id == 0:
                    gaQuery = "SELECT ga_id FROM GENETIC_ALGORITHMS WHERE selection=%s AND crossover=%s"
                    cursor.execute(gaQuery, (line[1], line[2]))
                    ga_id = cursor.fetchone()['ga_id']
                cnxn.commit()
                print(f'{ga_id}: {line[1]} selection with {line[2]} crossover inserted.')
                if len(sys.argv) > 1:
                    user = sys.argv[1]
                    cursor.execute("SELECT * FROM GA_DEFINES WHERE username=%s",(user))
                    maker = cursor.fetchone()
                    if maker is None:
                        definQ = "INSERT INTO GA_DEFINES(username, fk_ga_id) VALUES(%s,%s)"
                        cursor.execute(definQ, (user, ga_id))
                        cnxn.commit()
                continue

            #Insert line data into Neural Network
            query = "INSERT INTO NEURAL_NETWORKS(fitness, model, structure) VALUES ({0},'{1}','{2}')"
            query = query.format(line[4], line[2], line[3])
            cursor.execute(query)
            nnQuery = "SELECT LAST_INSERT_ID();"
            cursor.execute(nnQuery)
            nn_id = cursor.fetchone()['LAST_INSERT_ID()']
            cnxn.commit()

            #Insert the weight into Neural Network
            query = "INSERT INTO NN_WEIGHTS(fk_nn_id, weights) VALUES ({0},'{1}')"
            query = query.format(nn_id, line[5])
            cursor.execute(query)
            cnxn.commit()

            #Insert Line data in Population
            cnxn2 = pymysql.connect(host=os.getenv("MYSQL_SERVICE_HOST"), 
                  database=os.getenv("db_name"), 
                  user="root", 
                  password=os.getenv("db_root_password"),
                  charset="utf8mb4",
                  cursorclass=pymysql.cursors.DictCursor)
            tempcursor = cnxn2.cursor()

            query = "INSERT IGNORE INTO POPULATIONS({0},{1},{2}) VALUES ({3},'{4}',{5})"
            query = query.format("generation_number", "map_path", "fk_ga_id", line[0], line[1], ga_id)
            tempcursor.execute(query)
            popQuery = "SELECT LAST_INSERT_ID();"
            tempcursor.execute(popQuery)
            temp = tempcursor.fetchone()['LAST_INSERT_ID()']
            cnxn2.commit()
            cnxn2.close()
            pop_id = pop_id if temp == 0 else temp

            #Insert the Generation connection thing
            query = 'INSERT INTO GENERATIONS({0},{1}) VALUES ({2},{3})'
            query = query.format("indiv_id", "fk_pop_id", nn_id, pop_id)
            cursor.execute(query)
            cnxn.commit()

            counter += 1
    except Exception as e:
        print(f'An error has occurred! Changes will not be committed. Delete the GA if it still exists (id: {ga_id}).\n{e}', file=sys.stderr)
        delQuery = "DELETE FROM GENETIC_ALGORITHMS WHERE ga_id=%s"
        cursor.execute(delQuery,(ga_id,))
        cnxn.commit()
    finally:
        print(f"\n{counter - 2} row(s) inserted.")


cnxn.close()