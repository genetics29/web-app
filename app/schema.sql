-- Active: 1679696459550@@localhost@3306@genetics_db

DROP DATABASE IF EXISTS genetics_db;
CREATE DATABASE genetics_db;
USE genetics_db;

CREATE TABLE IF NOT EXISTS USERS (
    username    VARCHAR(20) NOT NULL UNIQUE, 
    passphrase    VARCHAR(128) NOT NULL,
    superuser BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (username)
);

CREATE TABLE IF NOT EXISTS GENETIC_ALGORITHMS (
    ga_id   INT NOT NULL AUTO_INCREMENT,
    crossover   VARCHAR(20) NOT NULL,
    selection   VARCHAR(20) NOT NULL,
    max_generations INT NOT NULL,
    mutation_scale  DEC(65,5) NOT NULL,
    mutation_rate   DEC(65,5) NOT NULL,
    population_count INT NOT NULL,
    elitist_amt INT NOT NULL,
    parent_amt  INT NOT NULL,
    CONSTRAINT CHK_GA_SELECTION CHECK (find_in_set(selection, 'roulette,elitist') > 0),
    CONSTRAINT CHK_GA_CROSSOVER CHECK (find_in_set(crossover, 'single_point,uniform_binary,custom') > 0),
    UNIQUE (crossover, selection),
    PRIMARY KEY (ga_id)
);

CREATE TABLE IF NOT EXISTS NEURAL_NETWORKS (
    nn_id INT NOT NULL AUTO_INCREMENT,
    model   VARCHAR(20) NOT NULL,
    structure   VARCHAR(20) NOT NULL,
    fitness DEC(65,10) NOT NULL,
    CONSTRAINT CHK_NN_MODEL CHECK (find_in_set(model, 'FNN,Pruned,NEAT') > 0),
    PRIMARY KEY (nn_id)
);

CREATE TABLE IF NOT EXISTS GA_DEFINES (
    username    VARCHAR(20) NOT NULL,
    fk_ga_id INT NOT NULL,
    PRIMARY KEY(username, fk_ga_id),
    UNIQUE(fk_ga_id),
    FOREIGN KEY(username)  
        REFERENCES USERS(username) 
        ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY(fk_ga_id)  
        REFERENCES GENETIC_ALGORITHMS(ga_id)  
        ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS POPULATIONS (
    pop_id INT NOT NULL AUTO_INCREMENT,
    generation_number INT NOT NULL,
    fk_ga_id INT NOT NULL,
    map_path VARCHAR(125) NOT NULL,
    UNIQUE(generation_number, fk_ga_id),
    PRIMARY KEY (pop_id),
    FOREIGN KEY (fk_ga_id)
        REFERENCES GENETIC_ALGORITHMS(ga_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT CHK_POP_NUM CHECK (generation_number >= 0)
);

CREATE TABLE IF NOT EXISTS GENERATIONS (
    indiv_id INT NOT NULL,
    fk_pop_id INT NOT NULL,
    PRIMARY KEY (indiv_id),
    FOREIGN KEY (fk_pop_id)
        REFERENCES POPULATIONS(pop_id)
        ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (indiv_id)
        REFERENCES NEURAL_NETWORKS(nn_id)
        ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE IF NOT EXISTS NN_DEFINES (
    username    VARCHAR(20) NOT NULL,
    fk_nn_id INT NOT NULL,
    PRIMARY KEY(username, fk_nn_id),
    FOREIGN KEY(username)  
        REFERENCES USERS(username)  
        ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY(fk_nn_id)  
        REFERENCES NEURAL_NETWORKS(nn_id)  
        ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE TABLE IF NOT EXISTS NN_WEIGHTS (
    weight_id INT NOT NULL AUTO_INCREMENT,
    fk_nn_id INT NOT NULL,
    weights MEDIUMTEXT NOT NULL,
    PRIMARY KEY(weight_id),
    UNIQUE(fk_nn_id),
    FOREIGN KEY(fk_nn_id)
        REFERENCES NEURAL_NETWORKS(nn_id)
        ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO USERS(username, passphrase, superuser) VALUES("admin", "914cb64a1bfac4c6f9507c9d26b81ed91757fa051e3f8508140053834d1dda81", TRUE);