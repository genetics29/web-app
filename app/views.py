import functools
import hashlib as hash
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from flask import abort

from app import mysql
from app.auth import login_required
from app.ga import getGAs
import sys

bp = Blueprint('stats', __name__, url_prefix='/app')

# -----------------------------------------------------------------------------------------

'''
Types of Queries:
- Best individual from population: Given population id/ generation number, return the best individual
- Best individual from simulation: return best ind. from the highest generation number of a particular simulation
- a true login if given a username matches the associated password

- weights from individual
- Most recent pop / GA
- best fitness from each generation of simulation
'''

def best_nn_from_ga(gaID):
    '''
    gets the best individual from the genetic algorithm with the gaID given
    '''
    conn = mysql.connect()
    cursor = conn.cursor()

    query = "SELECT * FROM NEURAL_NETWORKS WHERE fitness IN ( \
        SELECT MAX(fitness) from NEURAL_NETWORKS WHERE nn_id IN ( \
            SELECT indiv_id FROM GENERATIONS WHERE pop_num IN (\
                SELECT generation_number from POPULATIONS WHERE fk_ga_id={gaID}}))) LIMIT 1;"
    cursor.execute(query)

    return cursor.fetchall()

    
def best_nn_from_pop(popID):
    '''
    gets all the individuals with the best score from the population of the given popID
    '''
    conn = mysql.connect()
    cursor = conn.cursor()

    query = "SELECT * FROM NEURAL_NETWORKS WHERE fitness IN (\
        SELECT MAX(fitness) from NEURAL_NETWORKS WHERE nn_id IN (\
            SELECT indiv_id FROM GENERATIONS WHERE pop_num={popID})) LIMIT 1;"
    cursor.execute(query)

    return cursor.fetchall()


def fitness_from_ga(gaID):
    '''
    gets the best fitness from each generation in a simulation
    '''
    conn = mysql.connect()
    cursor = conn.cursor()

    query = "SELECT fitness from NEURAL_NETWORKS WHERE nn_id IN ( \
        SELECT DISTINCT indiv_id FROM GENERATIONS WHERE pop_num IN (\
            SELECT generation_number from POPULATIONS WHERE fk_ga_id={gaID}})) ORDER BY nn_id;"
    cursor.execute(query)

    return cursor.fetchall()


#note: if the ID is passed in as an integer, do we need to convert this into a string int he query string?
def get_weights(id):
    '''
    gets the weights from an individual via individual specifing ID
    '''
    conn = mysql.connect()
    cursor = conn.cursor()

    query = "SELECT weights FROM individuals WHERE id ={id};"
    cursor.execute(query)

    return cursor.fetchall()

def get_maker(gaID):
    '''
    gets the best fitness from each generation in a simulation
    '''
    conn = mysql.connect()
    cursor = conn.cursor()

    query = "SELECT username from NEURAL_NETWORKS WHERE fk_ga_id={gaID}"
    cursor.execute(query)

    return cursor.fetchall()

    
@bp.route('stats')
@login_required
def stats():
    return render_template('ga/stats.html', GAs = getGAs())